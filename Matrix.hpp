#ifndef MATRIX_HPP
#define MATRIX_HPP

#include "Vector.hpp"


namespace math {


typedef Eigen::Matrix3d RotMatrix3D;

enum {COORDINATE_3D = 1, TRANSFORM_3D=2, HOMOGENEOUS_3D=3 };

class Transform3D;


template <int TYPE>
class HomMatrix
{
protected:

    RotMatrix3D _R;
    loVector3D _T;


public:
    enum{ MY_TYPE = TYPE};

    HomMatrix() { setIdentity(); }
    HomMatrix(const RotMatrix3D & r, const loVector3D & t): _R( r) , _T(t) { }
    HomMatrix(const RotMatrix3D & r): _R( r)  { }

    HomMatrix & operator*= ( HomMatrix const& m)
    {
        _R = m.rotation();
        _T = m.translation();
    }

    void setIdentity();

    void transposeRotation();

    const RotMatrix3D  &  rotation()           { return _R; }
    const loVector3D &  translation()          { return _T; }

    /**
   * @brief operator * Compose together two Transform3D
   */
    HomMatrix  operator*( Transform3D & m);

    /**
   * @brief operator * Apply a tranform to obtain a new transform.
   */
    void operator*= ( Transform3D & m);

    /**
   * @brief translate Create a new transformation applying a translation to this one.
   */
    void translate(loVector3D const& t);

    void rotate_X( double angle);
    void rotate_Y( double angle);
    void rotate_Z( double angle);

    /**
   * @brief translate Create a new transformation applying a rotation to this one.
   */
    void rotate(double const& roll, double const&  pitch, double const&  yaw);

    void print();

    double & T(unsigned const& i)   { assert(i<3); return _T(i); }
    double & R(unsigned const& i)   { assert(i<9); return _R(i); }

    virtual int type() {return TYPE;}

};
//---------------------------------------------------------------------

class  Transform3D: public HomMatrix<TRANSFORM_3D>
{
    typedef HomMatrix Base;

    enum{ MY_TYPE = TRANSFORM_3D};
    //  Transform3D(const HomMatrix & v): HomMatrix(v)  { }

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    Transform3D(): HomMatrix() {}
    Transform3D(const RotMatrix3D & r, const loVector3D & t): HomMatrix(r,t) { }

    Transform3D( Transform3D   const& c);

    /**
   * @brief inverse inverse the current transform.
   */
    Transform3D inverse();


    void invert();

    template <class OtherType>
    Transform3D & operator= (const OtherType & other)
    {
        STATIC_ASSERT( (int)OtherType::MY_TYPE == (int)MY_TYPE, CONVERTION_IS_SEMANTICALLY_WRONG);
        this->Base::operator=(other);
        return *this;
    }

};
//---------------------------------------------------------------------

class  Coordinate3D: public HomMatrix<COORDINATE_3D>
{

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    Coordinate3D(): HomMatrix() {}

    Coordinate3D( Coordinate3D const& c): HomMatrix(c) {}

    //  Coordinate3D& operator=( Coordinate3D const& c);

    glVector3D operator*( const loVector3D & v);

};


//----------------------------------------------------------------

template <int TYPE>
inline void HomMatrix<TYPE>::setIdentity()
{
    _R(0) = 1.0;   _R(3) = 0.0;   _R(6) = 0.0;
    _R(1) = 0.0;   _R(4) = 1.0;   _R(7) = 0.0;
    _R(2) = 0.0;   _R(5) = 0.0;   _R(8) = 1.0;
    _T.setZero();
}

template <int TYPE>
inline void HomMatrix<TYPE>::print()
{
    std::cout.setf( std::ios::fixed, std:: ios::floatfield );
    std::cout.precision(2);
    std::cout << _R(0)<<"\t\t"<< _R(3)<<"\t\t" << _R(6)<<"\t|\t  " << _T(0) << std::endl;
    std::cout << _R(1)<<"\t\t"<< _R(4)<<"\t\t" << _R(7)<<"\t|\t  " << _T(1) << std::endl;
    std::cout << _R(2)<<"\t\t"<< _R(5)<<"\t\t" << _R(8)<<"\t|\t  " << _T(2) << std::endl << std::endl;
}

template <int TYPE>
inline HomMatrix<TYPE>  HomMatrix<TYPE>::operator*( Transform3D & m)
{
    RotMatrix3D      rot( _R* m.rotation() );
    Eigen::Vector3d  tra (  _R*m.translation() );
    return HomMatrix<TYPE>( rot, loVector3D(tra(0) + _T(0), tra(1)+ _T(1), tra(2)+ _T(2)));
}

template <int TYPE>
inline void  HomMatrix<TYPE>::operator*=( Transform3D & m)
{
    RotMatrix3D    rot(   _R* m.rotation() );
    Eigen::Vector3d      tra (  _R*m.translation() );

    _R = rot;
    _T(0) += tra(0);
    _T(1) += tra(1);
    _T(2) += tra(2);
}

inline RotMatrix3D Rotation_X(double rad_angle)
{
    RotMatrix3D rot;
    double cs = cos(rad_angle);
    double sn = sin(rad_angle);
    rot(0) = 1.0;     rot(3) = 0.0;     rot(6) = 0.0;
    rot(1) = 0.0;     rot(4) = cs;      rot(7) = -sn;
    rot(2) = 0.0;     rot(5) = sn;      rot(8) =  cs;
    return (rot);
}

inline RotMatrix3D Rotation_Y(double rad_angle)
{
    RotMatrix3D rot;
    double cs = cos(rad_angle);
    double sn = sin(rad_angle);
    rot(0) = cs;      rot(3) = 0.0;     rot(6) = sn;
    rot(1) = 0.0;     rot(4) = 1.0;     rot(7) = 0.0;
    rot(2) = -sn;     rot(5) = 0.0;     rot(8) = cs;
    return rot;
}

inline RotMatrix3D Rotation_Z(double rad_angle)
{
    RotMatrix3D rot;
    double cs = cos(rad_angle);
    double sn = sin(rad_angle);
    rot(0) = cs;      rot(3) = -sn;      rot(6) = 0.0;
    rot(1) = sn;      rot(4) = cs;       rot(7) = 0.0;
    rot(2) = 0.0;     rot(5) = 0.0;      rot(8) = 1.0;
    return (rot);
}

//---------------------------------------------
template <int TYPE>
void HomMatrix<TYPE>::rotate_X( double angle)
{
    RotMatrix3D temp = _R* Rotation_X(angle);
    _R = temp;
}

template <int TYPE>
void HomMatrix<TYPE>::rotate_Y( double angle)
{
    RotMatrix3D temp = _R* Rotation_Y(angle);
    _R = temp;
}


template <int TYPE>
void HomMatrix<TYPE>::rotate_Z( double angle)
{
    RotMatrix3D temp = _R* Rotation_Z(angle);
    _R = temp;
}

template <int TYPE>
void HomMatrix<TYPE>::translate(const loVector3D &t)
{
    Eigen::Vector3d temp = _R*t;
    _T(0) += temp(0);
    _T(1) += temp(1);
    _T(2) += temp(2);
}

template<int TYPE>
inline void HomMatrix<TYPE>::transposeRotation()
{
    _R.transposeInPlace();
}


inline void Transform3D::invert()
{
    _R.transposeInPlace();
    Eigen::Vector3d t ( -(_R*_T ));
    _T << t.x() , t.y(), t.z();
}

inline Transform3D Transform3D::inverse()
{
    RotMatrix3D rot = _R.transpose();
    Eigen::Vector3d t ( -(rot*_T) );
    return  Transform3D( rot, loVector3D(t(0), t(1), t(2) ) );
}

}
#endif // MATRIX_HPP
