#include <iostream>
#include "Vector.hpp"
#include "Matrix.hpp"
#include <Eigen/Geometry>

using namespace std;
using namespace math;

long long unsigned GetTime()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (ts.tv_sec* 1000000 + ts.tv_nsec/1000);
}


inline void DK_A(double *ang)
{
    Eigen::Affine3d foot;
    foot.setIdentity();

    (foot) *= Eigen::AngleAxisd(ang[0], Eigen::Vector3d(0,0,1) );
    (foot) *= Eigen::AngleAxisd(ang[1], Eigen::Vector3d(1,0,0) );
    (foot) *= Eigen::AngleAxisd(ang[2], Eigen::Vector3d(0,1,0) );

    foot.translate(Eigen::Vector3d(0.0, 0.0, -(0.3)));

    (foot) *= Eigen::AngleAxisd(ang[3], Eigen::Vector3d(0,1,0) );

    foot.translate(Eigen::Vector3d(0.0, 0.0, -(0.3)));
    (foot) *= Eigen::AngleAxisd(ang[4], Eigen::Vector3d(0,1,0) );
    (foot) *= Eigen::AngleAxisd(ang[5], Eigen::Vector3d(1,0,0) );

    foot.translate(Eigen::Vector3d(0.0, 0.0, -(0.1)));
}

inline void DK_B(double *ang)
{
    Coordinate3D foot;
    foot.setIdentity();

    foot.rotate_Z(ang[0]);
    foot.rotate_X(ang[1]);
    foot.rotate_Y(ang[2]);

    foot.translate( loVector3D(0.0, 0.0, -(0.3)));

    foot.rotate_Y(ang[3]);

    foot.translate(loVector3D(0.0, 0.0, -(0.3)));
    foot.rotate_Y(ang[4]);
    foot.rotate_X(ang[5]);

    foot.translate(loVector3D(0.0, 0.0, -(0.1)));
}



int main()
{
    cout << "Hello Wssssorld!" << endl;

    long long unsigned now = GetTime();

    long REPEAT = 1000000;



    for (long i=0; i< REPEAT; i++)
    {
        double ang[6]={1,2,3,4,5,6};
        DK_A(ang);
       /* Eigen::Vector3d e1,e2,e3;

        e1 << 1 ,2 ,3;
        e2 << 11 ,22 ,33;
        e3 = e1+e2;*/
    }

    printf("eigen: %lld\n",  GetTime() - now);


    now = GetTime();


    for (long i=0; i< REPEAT; i++)
    {
        double ang[6]={1,2,3,4,5,6};
        DK_B(ang);
       /* loVector3D f1,f2,f3;

        f1.set( 1 ,2 ,3 );
        f2.set( 11 ,22 ,33 );
        f3 = f1+f2;*/
        //e1 + e2;
    }
    printf("mine:  %lld\n",  GetTime() - now);


    glVector3D g1, g2, g3;
    loVector3D l1, l3;

    l1 = loVector3D( 1,2,3);

    loVector3D l2(10,-10,10);

    l3 = l1;
    g3 = g1;

    l3 = l1 +l2;
    g3 = g1 + l1;
    g3 = l1 + g1;

    l3 = l1 -l2;
    g3 = g1 - l1;
    g3 = l1 - g1;

    printf("--------\n");

    l1 = l1*5;


 //   g2 = g1*2;      // MUST fail!!
   //  l1 = g1;       // MUST fail!!
 //   g1 = l1;       // MUST fail!!
//    g2 = l1 + l2;  // MUST fail!!

   //  g3 = g1 + g2;  // MUST fail!!
  //   g3 = g1 - g2;  // MUST fail!!

    cout << l1 << endl;
    cout << l2 << endl;
 //   cout << l1 + l2*100 << endl;

    printf("--------\n");

    Coordinate3D c1;

    Transform3D t1, t2;

    t1.rotate_X( 0.5 );
    t1.translate( loVector3D( 1,2,3));

    t2 = t1;
    t2 = t1.inverse();

    printf("--------\n");
    c1.print();
    printf("--------\n");
    c1 *= t1;
    c1.print();
    printf("--------\n");
    c1 *= t2;
    c1.print();

/*//
  //  hm.print();

    Transform3D t1, t2;

    t1.print();
     printf("---- rot ----\n");

    t1.rotate_X( 0.5);

    t1.print();

    t2.print();
    t2 = (t2 * t1);

    t2 = t1;
    //t2 = c1; // MUST fail!!

    t2.print();
*/

    return 1;
}

