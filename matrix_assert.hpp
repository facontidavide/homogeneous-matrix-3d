#ifndef MATRIX_ASSERT_HPP
#define MATRIX_ASSERT_HPP

template<bool condition>
struct static_assertion {};

template<>
struct static_assertion<true>
{
    enum {
        CONVERTION_IS_SEMANTICALLY_WRONG,
        GLOBAL_VECTORS_CANT_BE_SCALED,
        CANT_DO_THIS_ON_TWO_GLOBAL_VECTORS
    };
};

#define STATIC_ASSERT(CONDITION,MSG)  if (static_assertion<bool(CONDITION)>::MSG) {}

#ifndef NDEBUG
#   define ASSERT(condition, message) \
    do { \
        if (! (condition)) { \
            std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
                      << " line " << __LINE__ << ": " << message << std::endl; \
            std::exit(EXIT_FAILURE); \
        } \
    } while (false)
#else
#   define ASSERT(condition, message) do { } while (false)
#endif

// You can use it this way
//
// ASSERT((0 < x) && (x < 10), "x was " << x);
//



#endif // MATRIX_ASSERT_HPP
