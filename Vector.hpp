#ifndef VECTOR_HPP
#define VECTOR_HPP


#include <Eigen/Core>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <boost/shared_ptr.hpp>
#include <boost/static_assert.hpp>


namespace math {

template<bool condition>
struct static_assertion {};

template<>
struct static_assertion<true>
{
    enum {
        CONVERTION_IS_SEMANTICALLY_WRONG,
        GLOBAL_VECTORS_CANT_BE_SCALED,
        TWO_GLOBAL_VECTORS_CANT_DO_THIS
    };
};


#define STATIC_ASSERT(CONDITION,MSG)  if (static_assertion<bool(CONDITION)>::MSG) {}

//-----------------------------------------------------------------------
class Coordinate3D;
class glVector3D;
class loVector3D;
//-----------------------------------------------------------------------
enum{ LOCAL_VECTOR = 10, GLOBAL_VECTOR=11};


template <int TYPE>
class Array3D: public Eigen::Vector3d
{

protected:

    Array3D(const Eigen::Vector3d & other): Eigen::Vector3d(other) {}

public:
    enum { MY_TYPE = TYPE };
    typedef Eigen::Vector3d Base;

    inline Array3D(): Eigen::Vector3d() { /*this->setZero();*/ }
    inline Array3D(double const& x,double const& y,double const& z): Eigen::Vector3d(x,y,z) {}

    inline void set(double const& x,double const& y,double const& z)
    {
        (*this) << x,y,z;
    }

    template <class OtherType>
    Array3D(OtherType const& v)
    {
        printf("1\n");
        STATIC_ASSERT( OtherType::MY_TYPE==MY_TYPE,    CONVERTION_IS_SEMANTICALLY_WRONG);
        (*this) = v;
    }

    template <class OtherType>
    Array3D & operator= (const OtherType & other)
    {
        printf("1\n");
        STATIC_ASSERT( OtherType::MY_TYPE==MY_TYPE,    CONVERTION_IS_SEMANTICALLY_WRONG);
        this->Base::operator=(other);
        return *this;
    }


};






class   loVector3D: public Array3D<LOCAL_VECTOR>
{


public:
#ifndef NDEBUG
    boost::shared_ptr<Coordinate3D> parent;
#endif

    typedef Eigen::Vector3d Base;
    enum {MY_TYPE = LOCAL_VECTOR };

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    loVector3D () : Array3D()                                       { }
    loVector3D (loVector3D const& v)
    {
#ifndef NDEBUG
        parent = v.parent;
#endif
        (*this) = v;
    }
    loVector3D(double const& x,double const& y,double const& z): Array3D(x,y,z) {}


    inline loVector3D operator + (const loVector3D & v);
    inline loVector3D operator - (const loVector3D & v);

    glVector3D operator + (const glVector3D & v);
    glVector3D operator - (const glVector3D & v);

    loVector3D& operator += (const loVector3D & v);
    loVector3D& operator -= (const loVector3D & v);

    loVector3D operator  *  (const double & v);
    loVector3D& operator *= (const double & v);

    void normalize();
    const loVector3D normalized() const;

    template <class OtherType>
    loVector3D(const OtherType & other)
    {
        STATIC_ASSERT( (int)OtherType::MY_TYPE==(int)MY_TYPE,    CONVERTION_IS_SEMANTICALLY_WRONG);
        (*this) << other.x() , other.y() , other.z();
    }

    loVector3D& operator= (const loVector3D & other)
    {
        (*this) << other.x() , other.y() , other.z();
        return *this;
    }


    template <class OtherType>
    loVector3D & operator= (const OtherType & other)
    {
        STATIC_ASSERT( (int)OtherType::MY_TYPE==(int)MY_TYPE,    CONVERTION_IS_SEMANTICALLY_WRONG);
        this->Base::operator=(other);
        return *this;
    }

private:
    // if you get the error  "math::loVector3D::loVector3D(const Eigen::Vector3d&)' is private "
    // you are doing somethig sematically WRONG

    loVector3D(const Eigen::Vector3d & v): Array3D(v) { }


};


class glVector3D: public Array3D<GLOBAL_VECTOR>
{

public:
    typedef Eigen::Vector3d Base;

    enum {MY_TYPE = GLOBAL_VECTOR };

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    glVector3D(): Array3D() {}

    template <class OtherType>
    glVector3D operator + (const OtherType & v);

    template <class OtherType>
    glVector3D operator - (const OtherType & v);

    template <class OtherType>
    glVector3D(OtherType const& v)
    {
        STATIC_ASSERT( (int)OtherType::MY_TYPE==(int)MY_TYPE,    CONVERTION_IS_SEMANTICALLY_WRONG);
        (*this) = v;
    }

    template <class OtherType>
    glVector3D & operator= (const OtherType & other)
    {
        STATIC_ASSERT( (int)OtherType::MY_TYPE==(int)MY_TYPE,    CONVERTION_IS_SEMANTICALLY_WRONG);
        this->Base::operator=(other);
        return *this;
    }

private:
    // if you get the error  "math::loVector3D::loVector3D(const Eigen::Vector3d&)' is private "
    // you are doing somethig sematically WRONG

    friend class loVector3D;

    glVector3D(const Eigen::Vector3d & v): Array3D(v) {}
};

//-------------------------------------------------------------------------------
//-------------------------------- INLINE IMPLEMENTATION ------------------------
//-------------------------------------------------------------------------------

std::ostream & 	operator<< (std::ostream &s, const loVector3D &v )
{
    return s << Eigen::Vector3d(v).transpose();
}

std::ostream & 	operator<< (std::ostream &s, const glVector3D &v )
{
    return s << Eigen::Vector3d(v).transpose();
}



inline loVector3D & loVector3D::operator += (const loVector3D & v)
{
    x()+=v.x();
    y()+=v.y();
    z()+=v.z();
    return *this;
    // return loVector3D( x()+v.x(), y()+v.y() , z()+v.z()  );
}

inline loVector3D & loVector3D::operator -= (const loVector3D & v)
{
    x()-=v.x();
    y()-=v.y();
    z()-=v.z();
    return *this;
}

inline loVector3D loVector3D::operator+ (const loVector3D & v)
{
    return loVector3D( x()+v.x(), y()+v.y() , z()+v.z()  );
}

inline loVector3D loVector3D::operator - (const loVector3D & v)
{
    return loVector3D( x()-v.x(), y()-v.y() , z()-v.z()  );
}


inline glVector3D loVector3D::operator + (const glVector3D & v)
{
    return Eigen::Vector3d(this->Base::operator+(v) ); // implemented by eigen
    // return loVector3D( x()+v.x(), y()+v.y() , z()+v.z()  );
}

inline glVector3D loVector3D::operator - (const glVector3D & v)
{
    return Eigen::Vector3d( this->Base::operator-(v) ) ; // implemented by eigen
    // return loVector3D( x()-v.x(), y()-v.y() , z()-v.z()  );
}

template <class OtherType>
inline glVector3D glVector3D::operator + (const OtherType & v)
{
    STATIC_ASSERT( (int)OtherType::MY_TYPE==LOCAL_VECTOR,    TWO_GLOBAL_VECTORS_CANT_DO_THIS);
    return Eigen::Vector3d(this->Base::operator+(v) ); // implemented by eigen
    // return loVector3D( x()+v.x(), y()+v.y() , z()+v.z()  );
}

template <class OtherType>
inline glVector3D  glVector3D::operator - (const OtherType & v)
{
    STATIC_ASSERT( (int)OtherType::MY_TYPE==LOCAL_VECTOR,    TWO_GLOBAL_VECTORS_CANT_DO_THIS);
    return Eigen::Vector3d(this->Base::operator-(v)) ; // implemented by eigen
    // return loVector3D( x()-v.x(), y()-v.y() , z()-v.z()  );
}


loVector3D  loVector3D::operator *(const double & v)
{
    return Eigen::Vector3d(this->Base::operator*(v)) ; // implemented by eigen
}

loVector3D&  loVector3D::operator *= (const double & v)
{
    this->Base::operator*=(v) ; // implemented by eigen
    return *this;
}

}// end namespace

#endif // VECTOR_HPP
